from django.contrib import admin
from content.models import Description, Activity

admin.site.register(Description)
admin.site.register(Activity)
