from django.contrib import admin
from teacher.models import Teacher, Needs

admin.site.register(Teacher)
admin.site.register(Needs)
